import { Button, Form, Input, message } from "antd";
import React from "react";
import bg_animate from "../../assets/lf30_editor_pafsbtzu.json";
import Lottie from "lottie-react";
import { postSignUp } from "../../Services/userService";
import { NavLink } from "react-router-dom";
export default function SignUpPage() {
    const onFinish = (values) => {
        console.log("Success:", values);
        postSignUp(values)
            .then((res) => {
                message.success("Đăng ký thành công!");
                console.log(res);
            })
            .catch((err) => {
                message.error("Đăng ký thất bại!");
                console.log(err);
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <div>
            <div className="w-screen h-screen p-20 flex justify-center items-center bg-blue-400">
                <div className="  p-3 rounded bg-white flex">
                    <div className="lg:w-1/2 xs:w-0">
                        <Lottie animationData={bg_animate} loop={true} />
                    </div>
                    <div className="lg:w-1/2 xs:w-full lg:pt-28 font-medium lg:pl-32">
                        <Form
                            layout="vertical"
                            name="basic"
                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 20,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                        >
                            <Form.Item
                                label="Tài Khoản"
                                name="taiKhoan"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your username!",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Mật khẩu"
                                name="matKhau"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your password!",
                                    },
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your email!",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Số Điện Thoại"
                                name="soDt"
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please input your Phone Number!",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Mã Nhóm"
                                name="maNhom"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your group Id!",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Họ và Tên"
                                name="hoTen"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your Fullname!",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                wrapperCol={{
                                    span: 20,
                                }}
                                className="text-center"
                            >
                                <Button
                                    className="bg-blue-500 hover:text-white mr-5"
                                    type="primary"
                                    htmlType="submit"
                                >
                                    Đăng Ký
                                </Button>
                                <NavLink to={"/login"}>
                                    <Button className="xs:mt-2">
                                        Đã có tài khoản? Đăng nhập tại đây!
                                    </Button>
                                </NavLink>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    );
}
