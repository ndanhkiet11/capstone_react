import { https } from "./configURL";

export let postLogin = (data) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};
// admin005 admin0031

export let postSignUp = (data) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", data);
};
