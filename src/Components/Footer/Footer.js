import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import FooterDesktop from "./FooterDesktop";
import FooterMobile from "./FooterMobile";
// import FooterTablet from "./FooterTablet";

export default function Footer() {
    return (
        <div>
            <Desktop>
                <FooterDesktop />
                <FooterMobile />
            </Desktop>
            <Tablet>
                <FooterMobile />
            </Tablet>
            <Mobile>
                <FooterMobile />
            </Mobile>
        </div>
    );
}
