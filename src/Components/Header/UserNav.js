import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../Services/localService";
import { Button } from "antd";

export default function UserNav() {
    let user = useSelector((state) => {
        return state.userReducer.user;
    });
    const handleLogOut = () => {
        userLocalService.remove();
        window.location.reload();
    };
    const renderContent = () => {
        if (user) {
            return (
                <>
                    <span className="xs:border-none border-r-2 border-gray-300  p-2 font-medium text-gray-600">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className=" h-8 mr-2 inline text-gray-500 "
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                        </svg>
                        {user?.hoTen}
                    </span>

                    <Button
                        type="primary"
                        onClick={handleLogOut}
                        danger
                        className="font-medium xs:mt-3 lg:mt-0"
                    >
                        Đăng xuất
                    </Button>
                </>
            );
        } else {
            return (
                <>
                    <NavLink to={"/login"}>
                        <Button className="font-medium" type="primary" danger>
                            Đăng nhập
                        </Button>
                    </NavLink>

                    <NavLink to={"/signup"}>
                        <Button className="font-medium" danger>
                            Đăng ký
                        </Button>
                    </NavLink>
                </>
            );
        }
    };
    return <div className="space-x-3">{renderContent()}</div>;
}
