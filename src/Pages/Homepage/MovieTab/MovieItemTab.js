import React from "react";
import moment from "moment/moment";

export default function MovieItemTab({ movie }) {
    return (
        <div className="flex mb-10 space-x-4">
            <img
                className="h-40 w-28 object-cover"
                src={movie.hinhAnh}
                alt=""
            />
            <div>
                <h2 className="font-medium text-xl">
                    <span className="px-1 bg-red-500 text-white rounded text-lg mr-1">
                        C18
                    </span>
                    {movie.tenPhim}
                </h2>
                <div className="grid  grid-cols-2  gap-5 mt-5 text-base text-gray-400">
                    {movie.lstLichChieuTheoPhim.slice(0, 4).map((lichChieu) => {
                        return (
                            <span className="bg-gray-100  font-medium px-3 py-2 rounded border border-gray-300">
                                <span className="text-green-700">
                                    {moment(lichChieu.ngayChieuGioChieu).format(
                                        "DD-MM-YYYY "
                                    )}
                                </span>
                                ~
                                <span className="text-red-500">
                                    {moment(lichChieu.ngayChieuGioChieu).format(
                                        " HH:MM"
                                    )}
                                </span>
                            </span>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}
