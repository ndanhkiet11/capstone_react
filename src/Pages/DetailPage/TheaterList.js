import { getTheaterByMovieId } from "../../Services/movieService";
import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Tabs } from "antd";
const onChange = (key) => {
    console.log(key);
};

export default function TheaterList({ movieId }) {
    const [dataMovie, setDataMovie] = useState([]);
    let params = useParams();
    useEffect(() => {
        getTheaterByMovieId(params.id)
            .then((res) => {
                console.log(res);
                setDataMovie(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [params.id]);
    const renderNgayChieu = (lichChieuPhim) => {
        return lichChieuPhim.map((item) => {
            return (
                <span className="bg-gray-100  font-medium px-3 py-2 rounded border border-gray-300 lg:w-1/3 2xl:w-1/4 text-center ">
                    <span className="text-green-700">
                        {moment(item.ngayChieuGioChieu).format("DD-MM-YYYY ")}
                    </span>
                    ~
                    <span className="text-red-500">
                        {moment(item.ngayChieuGioChieu).format(" HH:MM")}
                    </span>
                </span>
            );
            // return item.ngayChieuGioChieu;
        });
    };
    const renderLichChieuPhim = (cumRapChieu) => {
        return cumRapChieu.map((item) => {
            return (
                <div className="">
                    <h3 className="text-[#8BC34A] font-medium text-lg w-1/3">
                        {item.tenCumRap}
                    </h3>
                    <div className="container flex gap-5 mt-5 text-base text-gray-400 flex-wrap ">
                        {renderNgayChieu(item.lichChieuPhim)}
                    </div>
                </div>
            );
        });
    };
    const renderHeThongRap = () => {
        return dataMovie.heThongRapChieu?.map((heThongRap) => {
            return {
                label: (
                    <div>
                        <img
                            className="w-14 h-14"
                            src={heThongRap.logo}
                            alt=""
                        />
                    </div>
                ),

                key: heThongRap.maHeThongRap,
                children: renderLichChieuPhim(heThongRap.cumRapChieu),
            };
        });
    };

    // children: cumRap.cumRapChieu[0].tenCumRap,
    return (
        <div
            style={{ height: 600 }}
            className="container mx-auto px-12 xs:hidden  sm:hidden lg:block bg-white pt-5 "
        >
            <Tabs
                tabPosition="left"
                defaultActiveKey="1"
                items={renderHeThongRap()}
                onChange={onChange}
            />
        </div>
    );
}
