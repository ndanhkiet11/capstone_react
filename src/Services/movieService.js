import { https } from "./configURL";

export const getMovieList = () => {
    return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`);
};

export const getMovieByTheater = () => {
    return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
};

export const getMovieInfo = (movieId) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`);
};

export const getTheaterByMovieId = (movieId) => {
    return https.get(
        `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${movieId}`
    );
};

export const getMovieCarousel = () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
};
