import React, { useEffect, useState } from "react";
import { Desktop } from "../../HOC/Responsive";
import { getMovieList } from "../../Services/movieService";
import MovieCarousel from "./MovieCarousel";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";

export default function Homepage() {
    const [movieArr, setMovieArr] = useState([]);
    useEffect(() => {
        getMovieList()
            .then((res) => {
                console.log(res);
                setMovieArr(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <>
            <MovieCarousel />
            <div className="container sm:mx-auto px-3 2xl:px-36 xs:px-3">
                <MovieList movieArr={movieArr} />
                <Desktop>
                    <MovieTab />
                </Desktop>
            </div>
        </>
    );
}
