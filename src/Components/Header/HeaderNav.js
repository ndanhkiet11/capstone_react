import React from "react";

export default function HeaderNav() {
    return (
        <div className="font-medium lg:space-x-5 md:flex-col md:text-lg xs:text-xl flex lg:flex-row lg:text-lg  xs:flex-col xs:space-y-10 lg:space-y-0 justify-center">
            <a href="#lichChieu">Lịch Chiếu</a>
            <a href="#cumRap">Cụm Rạp</a>
            <a href="#footer">Thông Tin</a>
        </div>
    );
}
