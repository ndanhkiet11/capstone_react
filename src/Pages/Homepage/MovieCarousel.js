import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { getMovieCarousel } from "../../Services/movieService";

export default function MovieCarousel() {
    const [carouselItem, setCarouselItem] = useState([]);
    console.log("carouselItem: ", carouselItem);
    useEffect(() => {
        getMovieCarousel()
            .then((res) => {
                console.log(res);
                setCarouselItem(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    const setMovieCarousel = () => {
        return carouselItem.map((item) => {
            return (
                <div>
                    <img
                        className="lg:h-[700px] sm:h-[300px]"
                        style={{
                            // height: 700,
                            width: "100%",
                            objectFit: "cover",
                        }}
                        src={item.hinhAnh}
                        alt=""
                    />
                </div>
            );
        });
    };
    return (
        <div>
            <Carousel dots autoplay>
                {setMovieCarousel()}
            </Carousel>
        </div>
    );
}
