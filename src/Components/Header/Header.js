import React from "react";
import { NavLink } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import HeaderNav from "./HeaderNav";
import UserNav from "./UserNav";
import UserNavMenu from "./UserNavMenu";

export default function Header() {
    return (
        <div className="flex px-20 py-5 justify-between items-center shadow-lg sticky top-0 z-50 bg-white">
            <NavLink to={"/"}>
                <span className="text-red-600 text-3xl font-medium">
                    CyberFlix
                </span>
            </NavLink>
            <Desktop>
                <HeaderNav />
            </Desktop>
            <Desktop>
                <UserNav />
            </Desktop>
            <Tablet>
                <UserNavMenu />
            </Tablet>
            <Mobile>
                <UserNavMenu />
            </Mobile>
        </div>
    );
}
