import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../Services/movieService";
import { Tabs } from "antd";
import "./movieTab.css";
import MovieItemTab from "./MovieItemTab";
const onChange = (key) => {
    console.log(key);
};

export default function MovieTab() {
    const [dataMovie, setDataMovie] = useState([]);

    useEffect(() => {
        getMovieByTheater()
            .then((res) => {
                console.log(res);
                setDataMovie(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    const renderDsPhimTheoCumRap = (cumRap) => {
        return (
            <div>
                {cumRap.danhSachPhim.map((movie) => {
                    return <MovieItemTab movie={movie} />;
                })}
            </div>
        );
    };
    const renderCumRapTheoHeThongRap = (heThongRap) => {
        return heThongRap.lstCumRap.map((cumRap) => {
            return {
                label: (
                    <div className="w-52 ">
                        <h5 className="font-medium text-red-500">
                            {cumRap.tenCumRap}
                        </h5>
                        <p className="truncate ">{cumRap.diaChi}</p>
                    </div>
                ),
                key: cumRap.maCumRap,
                children: (
                    <div style={{ height: 600, overflowY: "scroll" }}>
                        {renderDsPhimTheoCumRap(cumRap)}
                    </div>
                ),
            };
        });
    };
    const renderHeThongRap = () => {
        return dataMovie.map((heThongRap) => {
            return {
                key: heThongRap.maHeThongRap,
                label: (
                    <img className=" w-14 h-14 " src={heThongRap.logo} alt="" />
                ),
                children: (
                    <Tabs
                        style={{ height: 600 }}
                        tabPosition="left"
                        defaultActiveKey="1"
                        onChange={onChange}
                        items={renderCumRapTheoHeThongRap(heThongRap)}
                    />
                ),
            };
        });
    };
    return (
        <div id="cumRap" className="container mx-auto mt-16 px-10 mb-5">
            <Tabs
                animated
                tabPosition="left"
                defaultActiveKey="1"
                items={renderHeThongRap()}
                onChange={onChange}
            />
        </div>
    );
}
