import React from "react";
import { Button, Drawer, Space } from "antd";
import { useState } from "react";
import UserNav from "./UserNav";
import HeaderNav from "./HeaderNav";
export default function UserNavMenu() {
    const [open, setOpen] = useState(false);
    const [placement, setPlacement] = useState("left");
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Space>
                <Button danger className="border-none " onClick={showDrawer}>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={2.5}
                        stroke="currentColor"
                        className="w-8 h-8 "
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                        />
                    </svg>
                </Button>
            </Space>
            <Drawer
                placement={"left"}
                closable={false}
                onClose={onClose}
                open={open}
                key={placement}
                width={250}
            >
                <div>
                    <UserNav />
                    <div className="mt-10">
                        <HeaderNav />
                    </div>
                </div>
            </Drawer>
        </>
    );
}
