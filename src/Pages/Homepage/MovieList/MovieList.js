import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function MovieList({ movieArr }) {
    const renderMovieList = () => {
        return movieArr.slice(0, 15).map((item) => {
            return (
                <Card
                    hoverable
                    className="md:w-full xs:w-[500px] xs:flex md:flex-col mx-auto"
                    cover={
                        <img
                            className="h-60 sm:w-10 object-cover"
                            alt=""
                            src={item.hinhAnh}
                        />
                    }
                >
                    <Meta
                        className="h-24"
                        title={<h2 className="">{item.tenPhim}</h2>}
                        description={
                            <h2>
                                {item.moTa.length > 55
                                    ? item.moTa.slice(0, 55) + "..."
                                    : item.moTa}
                            </h2>
                        }
                    />
                    <NavLink
                        className=" bg-red-500 px-5 py-2 text-white hover:bg-red-600 hover:text-white rounded"
                        to={`/detail/${item.maPhim}`}
                    >
                        Xem chi tiết
                    </NavLink>
                </Card>
            );
        });
    };
    return (
        <div
            id="lichChieu"
            className=" grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 gap-2 py-5"
        >
            {renderMovieList()}
        </div>
    );
}
