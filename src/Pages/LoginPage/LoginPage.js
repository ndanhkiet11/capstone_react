import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../../Services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constants/userConstant";
import { userLocalService } from "../../Services/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/41812-christmas-tree.json";

export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = (values) => {
        console.log("Success:", values);
        postLogin(values)
            .then((res) => {
                console.log(res);
                message.success("Đăng nhập thành công!");
                dispatch({
                    type: SET_USER_LOGIN,
                    payload: res.data.content,
                });
                userLocalService.set(res.data.content);
                setTimeout(() => {
                    navigate("/");
                }, 1000);
            })
            .catch((err) => {
                console.log(err);
                message.error("Đăng nhập thất bại!");
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <div className="w-screen h-screen p-20  flex justify-center items-center bg-blue-400">
            <div className="   p-3 rounded bg-white flex">
                <div className="w-1/2  ">
                    <Lottie animationData={bg_animate} loop={true} />
                </div>
                <div className="w-1/2 lg:pt-40 sm:py-auto font-medium xs:w-[500px]">
                    <Form
                        layout="vertical"
                        name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Tài Khoản"
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your username!",
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Mật Khẩu"
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your password!",
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{
                                span: 24,
                            }}
                            className="text-center"
                        >
                            <Button
                                className="bg-blue-500 hover:text-white"
                                type="primary"
                                htmlType="submit"
                            >
                                Đăng nhập
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
