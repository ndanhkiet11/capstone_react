import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieInfo } from "../../Services/movieService";
import moment from "moment/moment";
import { Rate } from "antd";
import { Progress, Space } from "antd";
import TheaterList from "./TheaterList";
export default function DetailPage() {
    let params = useParams();
    const [movieInfo, setMovieInfo] = useState({});
    useEffect(() => {
        getMovieInfo(params.id)
            .then((res) => {
                console.log(res);
                setMovieInfo(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [params.id]);

    return (
        <div style={{ backgroundColor: "rgb(10, 32, 41)" }}>
            <div className="flex lg:px-48 sm:px-5  py-10 space-x-10 sm:justify-center">
                <div>
                    <img
                        style={{ height: 350, width: 250, objectFit: "cover" }}
                        src={movieInfo.hinhAnh}
                        alt=""
                    />
                </div>
                <div className="text-white font-medium flex flex-col justify-center lg:w-1/6 ">
                    <h4>
                        {moment(movieInfo.ngayKhoiChieu).format("DD.MM.YYYY")}
                    </h4>
                    <h2 className="text-xl">{movieInfo.tenPhim}</h2>

                    <button className="mt-5 bg-red-600 px-5 py-2 rounded hover:bg-red-700">
                        Mua vé
                    </button>
                    <Rate
                        className="lg:hidden"
                        allowHalf
                        disabled
                        defaultValue={5}
                        value={movieInfo.danhGia / 2}
                    />
                </div>
                <div className="flex flex-col justify-center items-end w-2/3 pr-40 xs:hidden lg:inline-flex">
                    <Space wrap>
                        <Progress
                            trailColor="#567189"
                            className="bg-black rounded-full"
                            strokeWidth={8}
                            strokeColor={"#86F348"}
                            type="circle"
                            percent={parseInt(movieInfo.danhGia) * 10}
                            format={(percent) => (
                                <span className="text-5xl text-white">
                                    {percent / 10}{" "}
                                </span>
                            )}
                        />
                    </Space>
                    <Rate
                        allowHalf
                        disabled
                        defaultValue={5}
                        value={movieInfo.danhGia / 2}
                    />
                </div>
            </div>
            <div className="w-3/5 mx-auto ">
                <TheaterList />
            </div>
        </div>
    );
}
