import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./HOC/Layout";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Homepage from "./Pages/Homepage/Homepage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import SignUpPage from "./Pages/SignUpPage/SignUpPage";

function App() {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={
                            <Layout>
                                <Homepage />
                            </Layout>
                        }
                    />
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/signup" element={<SignUpPage />} />
                    <Route
                        path="/detail/:id"
                        element={
                            <Layout>
                                <DetailPage />
                            </Layout>
                        }
                    />
                    <Route path="*" element={<NotFoundPage />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
